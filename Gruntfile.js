module.exports = function(grunt) {
    grunt.initConfig({
        sass: {
            dist: {
                options: {
                    style: 'expanded'
                },
                files: {
                    "css/skeleton.css": "scss/skeleton.scss"
                }
            }
        },

        watch: {
            options: {
                livereload: true,
            },
            styles: {
                files: ['scss/**/*.scss', 'index.html'], // which files to watch
                tasks: ['sass'],
                options: {
                    nospawn: true
                }
            }
        },

        browserSync: {
            dev: {
                bsFiles: {
                    src : [
                        './css/*.css',
                        './index.html'
                    ]
                },
                options: {
                    watchTask: true,
                    server: {
                      url: 'http://localhost:88',
                      app: 'Google Chrome'
                    }
                }
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-sass');
    grunt.registerTask('default', ['sass', 'browserSync', 'watch']);
};
